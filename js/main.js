import { State } from "./state.js";
import { World } from "./ecs.js";

const ENTRY_POINT_ID = "cat-petting-starts-here";

window.onload = () => {
	const entrypoint = document.getElementById(ENTRY_POINT_ID);

	let world = new World()
		.addSys("display", (data) => {})
		.addSys("log", (data, deps) => console.log(deps.display), "display");

	world.createEntity("player", {
		display: "@",
		log: Symbol(),
	});

	world.createEntity("cat", {
		display: "c",
		log: Symbol()
	});

	world.createEntity("tree", {
		display: "t",
		log: Symbol()
	});

	world.step();

	let state = new State(entrypoint, 10, 30);
	state.loop();
}

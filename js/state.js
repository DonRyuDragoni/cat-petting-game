import { AsciiMap } from "./map.js";
import { Enum } from "./enum.js";

const GameState = new Enum(
	"playing",

	"done",
).freeze();

const GameProgression = new Enum(
	"farm",
	"house",
	"industry",
).freeze();

const Score = class {
	constructor(pets = 0, uniqueCats = 0) {
		this.pets = pets;
		this.uniqueCats = uniqueCats;
	}
};

export const State = class {
	constructor(entrypoint, rows, cols) {
		const mapCanvas = document.createElement("pre");
		entrypoint.appendChild(mapCanvas);

		this.map = new AsciiMap(mapCanvas, rows, cols);
		this.state = GameState.playing;
		this.progression = GameProgression.farm;
		this.score = new Score();

		this.lastCallTime = Date.now();

		this.boundLoop = this.loop.bind(this);
	}

	loop() {
		if(this.state !== GameState.done) {
			const now = Date.now();
			const dt = now - this.lastCallTime;

			this.__loop(dt);
			this.lastCallTime = now;

			window.requestAnimationFrame(this.boundLoop);
		}
	}

	__loop(dt) {
		console.log(dt);
		this.render();
	}

	render() {
		this.map.render();
	}
};

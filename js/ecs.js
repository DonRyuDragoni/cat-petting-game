export const World = class {
	constructor() {
		// Array[String]
		this.entities = [];

		// map[component -> data]
		this.componentStorage = {};

		// map[component -> fn]
		this.systems = {};

		// map[component -> Array[component]]
		this.depends = {};
	}

	__hasSysFor(component) {
		return this.systems[component] !== undefined;
	}

	__checkComponentType(component) {
		if(typeof component !== "string") {
			throw `Component names can only be strings, ${typeof component} ` +
				"is not supported";
		}
	}

	__checkDuplicateSystem(component) {
		if(this.__hasSysFor(component)) {
			throw `Adding duplicate system for component ${component}`;
		}
	}

	// Add a new system to the world.
	//
	// Checks are made to make sure the system doesn't already exist.
	// Attempting to override an existing system will cause an exception.
	addSys(component, sysFn, ...depends) {
		this.__checkComponentType(component);
		this.__checkDuplicateSystem(component);

		// Check that each element in the dependency list is a string that
		// corresponds to an existing component name.
		depends.forEach((component) => {
			this.__checkComponentType(component);

			if(!this.__hasSysFor(component)) {
				throw "Attempting to add dependency with no system attached " +
					component;
			}
		});

		this.depends[component] = depends;
		this.systems[component] = sysFn;

		return this;
	}

	// Searches for an entity with a given id. Returns undefined in case it
	// finds nothing.
	search(id) {
		for(let entity of this.entities)
			if(entity === id)
				return entity;

		return undefined;
	}

	// Adds a new entity to the world.
	//
	// This function ensures that all components passed meet their dependencies
	// such that, if this succeed, then the state of the world is valid.
	// However, if this function throws, then you cannot trust the validity of
	// the current world.
	createEntity(id, components) {
		if(typeof id !== "string")
			throw `Expected string id type, got ${typeof id}`;

		if(this.search(id) !== undefined)
			throw `Entity with duplicate id: ${id}`;

		this.entities.push(id);
		const entity = this.entities[this.entities.length - 1];

		if(!this.componentStorage[entity]) this.componentStorage[entity] = {};

		// Blindly add all components without checking for dependencies, as
		// this will loop in arbitrary order.
		for(let component in components) {
			if(typeof components[component] === "undefined")
				throw "Attempting to add a component with undefined data. If" +
					" this component for this entity should hold no data," +
					" please use a Symbol()";

			if(!this.__hasSysFor(component))
				throw "Attempting to create entity with unknown component: " +
					component;

			this.componentStorage[entity][component] = components[component];
		}

		// Check that all dependencies were met for all components.
		for(let component in components) {
			this.depends[component].forEach((dep) => {
				if(!this.__getComponentStorage(entity, dep)) {
					throw `Unmet component dependency, ${component} depends` +
						` on ${dep}, which does not exist for this entity`;
				}
			})
		}
	}

	__getComponentStorage(entity, component) {
		return this.componentStorage[entity][component];
	}

	__getDependencyData(entity, component) {
		const dependComponents = this.depends[component];

		if(dependComponents.length === 0) return undefined;

		let depends = {};
		dependComponents.forEach((component) => {
			depends[component] = this.__getComponentStorage(entity, component);
		});

		return depends;
	}

	step() {
		for(let component in this.systems) {
			this.entities.forEach((entity) => {
				const data = this.__getComponentStorage(entity, component);
				const depends = this.__getDependencyData(entity, component);

				if(data !== undefined) this.systems[component](data, depends);
			})
		}
	}
}

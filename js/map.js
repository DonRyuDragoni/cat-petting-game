import { Enum } from "./enum.js";

/**
 * The possible background components of the map. Things such as players or
 * cats are expected to add themselves into the map by calling drawOver().
 *
 * These are supposed to be uninteractable objects in the world, thus are
 * treated differently.
 */
const MapElement = new Enum(
	"grass",
	"tree",
).freeze();

/**
 * The default mapping from MapElement to their corresponding display
 * characters.
 */
const defaultMap = (mapElem) => {
	switch(mapElem) {
	case MapElement.grass:
		return ",";
	case MapElement.tree:
		return "t";
	}
}

export const AsciiMap = class {
	/*
	 * Creates and initializes a map with grass and tree elements.
	 *
	 * - canvas: a reference to a "pre" element in the page;
	 *
	 * - rows, cols: the size of the map;
	 *
	 * - treeChance: an absolute probability that defines the chance of a given
	 *               square on the map to br turned into a tree (if undefined,
	 *               it'll be considered 0.02);
	 *
	 * - elementMap: a function that maps elements on the map (defined by the
	 *               MapElement enum) to their corresponding display characters
	 *               (if undefined, defaultMap will be used).
	 */
	constructor(
		canvas,
		rows, cols,
		treeChance = 0.02,
		elementMap = defaultMap
	) {
		this.canvas     = canvas;
		this.rows       = rows;
		this.cols       = cols;
		this.elementMap = elementMap;

		const chance = treeChance;

		this.map = Array(this.rows * this.cols).fill(MapElement.grass);
		this.map.forEach((el, idx) => {
			const rn = Math.random();

			if(rn < 0.02) {
				this.map[idx] = MapElement.tree;
			}
		});
	}

	__index(row, col) {
		return row * this.cols + col;
	}

	/**
	 * Returns the element in the desired position.
	 */
	get(row, col) {
		return this.map[this.__index(row, col)];
	}

	/**
	 * Call to redraw the map on the page. By default, it'll render the whole
	 * map in the "pre" element passed on the constructor.
	 */
	render(
		startr = 0,
		startc = 0,
		width  = this.cols,
		height = this.rows,
		canvas = this.canvas,
	) {
		let stringMap = "";

		let currCol = startc;
		let currRow = startr;

		while(currRow < height) {
			let el = this.get(currRow, currCol);
			stringMap += this.elementMap(el);

			++currCol;

			if(currCol >= width) {
				stringMap += "\n";
				currCol = startc;
				++currRow;
			}
		}

		canvas.innerHTML = stringMap;
	}
}

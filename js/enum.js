// Adding support for enumerations in JavaScript.
export const Enum = class {
	constructor(...args) {
		args.forEach((el, idx) => {
			console.assert(
				typeof el === "string",
				"Enum elements can only be strings",
			);

			this[el] = Symbol(el);
		});
	}

	freeze() {
		return Object.freeze(this);
	}
};

